#include <iostream>
using namespace std;

template <class T>

T bigger(T a, T b) {
  return (a > b)? a : b;
}
int main()
{
   cout << "bigger int : " << bigger(1,22) << endl;
   cout << "bigger of char : " << bigger('a', 'b') << endl;
   cout << "bigger of float : " << bigger(1.0, 3.3) << endl;

   return 0;
}

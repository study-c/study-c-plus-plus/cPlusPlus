#include <iostream>

using namespace std;

int main(void)
{
    int a = 3;
    int &b = a;
    const int &c = a;
    cout << b << endl; 
    std::cout << c << std::endl;
	cout << "aaa" << endl;

    a = 5;
    cout << b << endl; 
    {
        int a1 = 30;
        const int &a2 = a1;
        std::cout << a2 << std::endl;
    }
    {
        
    }
    return 0;
}

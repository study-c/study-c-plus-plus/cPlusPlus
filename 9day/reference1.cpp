#include <iostream>
#include <string>

using namespace std;

struct Teacher 
{
	int age;
	string name;
};

int main( int argc, char **argv )
{
	Teacher t1;
	t1.age = 33;
	t1.name = "jerry" ;
	const Teacher &t2 = t1;
	cout << t1.name << endl;
	cout << t2.name << endl;

	return 0;
}

int func (int i) {
    return i;
};

float func (float f) {
    return f;
};

template <class T>
T func(T t) {
    return t;
};

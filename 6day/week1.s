	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 13
	.globl	_main
	.p2align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Lcfi0:
	.cfi_def_cfa_offset 16
Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$568, %rsp              ## imm = 0x238
Lcfi3:
	.cfi_offset %rbx, -24
	leaq	-360(%rbp), %rax
	leaq	-384(%rbp), %rcx
	leaq	-440(%rbp), %rdx
	leaq	-320(%rbp), %rsi
	leaq	-432(%rbp), %rdi
	leaq	-272(%rbp), %r8
	leaq	-424(%rbp), %r9
	leaq	-224(%rbp), %r10
	leaq	-496(%rbp), %r11
	movl	$0, -460(%rbp)
	movl	$0, -468(%rbp)
	movq	%r11, -456(%rbp)
	movq	-456(%rbp), %r11
	movq	%r11, -448(%rbp)
	movq	-448(%rbp), %r11
	movq	%r11, -416(%rbp)
	movq	-416(%rbp), %r11
	movq	%r11, %rbx
	movq	%rbx, -408(%rbp)
	movq	%r10, -208(%rbp)
	movq	$-1, -216(%rbp)
	movq	-208(%rbp), %r10
	movq	-216(%rbp), %rbx
	movq	%r10, -192(%rbp)
	movq	%rbx, -200(%rbp)
	movq	-192(%rbp), %r10
	movq	$0, (%r10)
	movq	-224(%rbp), %r10
	movq	%r10, -424(%rbp)
	movq	%r9, -232(%rbp)
	movq	$0, (%r11)
	movq	%r8, -256(%rbp)
	movq	$-1, -264(%rbp)
	movq	-256(%rbp), %r8
	movq	-264(%rbp), %r9
	movq	%r8, -240(%rbp)
	movq	%r9, -248(%rbp)
	movq	-240(%rbp), %r8
	movq	$0, (%r8)
	movq	-272(%rbp), %r8
	movq	%r8, -432(%rbp)
	movq	%rdi, -280(%rbp)
	movq	$0, 8(%r11)
	addq	$16, %r11
	movq	%rsi, -304(%rbp)
	movq	$-1, -312(%rbp)
	movq	-304(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	%rsi, -288(%rbp)
	movq	%rdi, -296(%rbp)
	movq	-288(%rbp), %rsi
	movq	$0, (%rsi)
	movq	-320(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%r11, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	-392(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movq	%rdx, -376(%rbp)
	movq	%rsi, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	%rcx, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -344(%rbp)
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movl	$0, -464(%rbp)
LBB0_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$40, -464(%rbp)
	jge	LBB0_10
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
	leaq	-464(%rbp), %rax
	leaq	-496(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	-168(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	cmpq	(%rdx), %rcx
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	je	LBB0_4
## BB#3:                                ##   in Loop: Header=BB0_1 Depth=1
	leaq	-184(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-544(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	$1, -136(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	-544(%rbp), %rdx        ## 8-byte Reload
	movq	8(%rdx), %rsi
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -80(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movl	(%rsi), %r8d
	movl	%r8d, (%rcx)
	movq	%rax, -88(%rbp)
	movq	8(%rdx), %rax
	addq	$4, %rax
	movq	%rax, 8(%rdx)
	jmp	LBB0_6
LBB0_4:                                 ##   in Loop: Header=BB0_1 Depth=1
	movq	-176(%rbp), %rsi
Ltmp15:
	movq	-544(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIKiEEvRT_
Ltmp16:
	jmp	LBB0_5
LBB0_5:                                 ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_6
LBB0_6:                                 ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_7
LBB0_7:                                 ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_8
LBB0_8:                                 ##   in Loop: Header=BB0_1 Depth=1
	movl	-464(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -464(%rbp)
	jmp	LBB0_1
LBB0_9:
Ltmp17:
	movl	%edx, %ecx
	movq	%rax, -504(%rbp)
	movl	%ecx, -508(%rbp)
	jmp	LBB0_20
LBB0_10:
Ltmp0:
	leaq	-536(%rbp), %rdi
	leaq	-496(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
Ltmp1:
	jmp	LBB0_11
LBB0_11:
Ltmp2:
	leaq	-468(%rbp), %rdi
	leaq	-536(%rbp), %rsi
	callq	__Z3sumPiNSt3__16vectorIiNS0_9allocatorIiEEEE
Ltmp3:
	jmp	LBB0_12
LBB0_12:
Ltmp7:
	leaq	-536(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
Ltmp8:
	jmp	LBB0_13
LBB0_13:
Ltmp9:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str(%rip), %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp10:
	movq	%rax, -552(%rbp)        ## 8-byte Spill
	jmp	LBB0_14
LBB0_14:
	movl	-468(%rbp), %esi
Ltmp11:
	movq	-552(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp12:
	movq	%rax, -560(%rbp)        ## 8-byte Spill
	jmp	LBB0_15
LBB0_15:
	movq	-560(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -16(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	-24(%rbp), %rcx
Ltmp13:
	callq	*%rcx
Ltmp14:
	movq	%rax, -568(%rbp)        ## 8-byte Spill
	jmp	LBB0_16
LBB0_16:
	jmp	LBB0_17
LBB0_17:
	leaq	-496(%rbp), %rdi
	movl	$0, -460(%rbp)
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	movl	-460(%rbp), %eax
	addq	$568, %rsp              ## imm = 0x238
	popq	%rbx
	popq	%rbp
	retq
LBB0_18:
Ltmp4:
	movl	%edx, %ecx
	movq	%rax, -504(%rbp)
	movl	%ecx, -508(%rbp)
Ltmp5:
	leaq	-536(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
Ltmp6:
	jmp	LBB0_19
LBB0_19:
	jmp	LBB0_20
LBB0_20:
Ltmp18:
	leaq	-496(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
Ltmp19:
	jmp	LBB0_21
LBB0_21:
	jmp	LBB0_22
LBB0_22:
	movq	-504(%rbp), %rdi
	callq	__Unwind_Resume
LBB0_23:
Ltmp20:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -572(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset0 = Ltmp15-Lfunc_begin0             ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp1-Ltmp15                    ##   Call between Ltmp15 and Ltmp1
	.long	Lset1
Lset2 = Ltmp17-Lfunc_begin0             ##     jumps to Ltmp17
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp2-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp3-Ltmp2                     ##   Call between Ltmp2 and Ltmp3
	.long	Lset4
Lset5 = Ltmp4-Lfunc_begin0              ##     jumps to Ltmp4
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp7-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp14-Ltmp7                    ##   Call between Ltmp7 and Ltmp14
	.long	Lset7
Lset8 = Ltmp17-Lfunc_begin0             ##     jumps to Ltmp17
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp14-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp5-Ltmp14                   ##   Call between Ltmp14 and Ltmp5
	.long	Lset10
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset11 = Ltmp5-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Ltmp19-Ltmp5                   ##   Call between Ltmp5 and Ltmp19
	.long	Lset12
Lset13 = Ltmp20-Lfunc_begin0            ##     jumps to Ltmp20
	.long	Lset13
	.byte	1                       ##   On action: 1
Lset14 = Ltmp19-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset14
Lset15 = Lfunc_end0-Ltmp19              ##   Call between Ltmp19 and Lfunc_end0
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z3sumPiNSt3__16vectorIiNS0_9allocatorIiEEEE
	.weak_definition	__Z3sumPiNSt3__16vectorIiNS0_9allocatorIiEEEE
	.p2align	4, 0x90
__Z3sumPiNSt3__16vectorIiNS0_9allocatorIiEEEE: ## @_Z3sumPiNSt3__16vectorIiNS0_9allocatorIiEEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi4:
	.cfi_def_cfa_offset 16
Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi6:
	.cfi_def_cfa_register %rbp
	subq	$200, %rsp
	leaq	-312(%rbp), %rax
	leaq	-304(%rbp), %rcx
	leaq	-40(%rbp), %rdx
	leaq	-296(%rbp), %r8
	movq	%rdi, -288(%rbp)
	movq	%r8, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -272(%rbp)
	movq	-288(%rbp), %rdi
	movl	$0, (%rdi)
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	(%rdi), %r8
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rdi
	movq	%rdx, -8(%rbp)
	movq	%rdi, -16(%rbp)
	movq	-8(%rbp), %rdx
	movq	-16(%rbp), %rdi
	movq	%rdi, (%rdx)
	movq	-40(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rdx, -312(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -128(%rbp)
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-304(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rsi, -328(%rbp)        ## 8-byte Spill
LBB1_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-320(%rbp), %rax
	leaq	-296(%rbp), %rcx
	leaq	-168(%rbp), %rdx
	movq	-328(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -200(%rbp)
	movq	-200(%rbp), %rdi
	movq	8(%rdi), %r8
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	-184(%rbp), %rdi
	movq	%rdx, -152(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rdi
	movq	%rdi, (%rdx)
	movq	-168(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -320(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%rax, -248(%rbp)
	movq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	(%rax), %rax
	movq	-232(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%r9b
	xorb	$-1, %r9b
	testb	$1, %r9b
	jne	LBB1_2
	jmp	LBB1_4
LBB1_2:                                 ##   in Loop: Header=BB1_1 Depth=1
	leaq	-296(%rbp), %rax
	movq	-288(%rbp), %rcx
	movl	(%rcx), %edx
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	(%rax), %rax
	addl	(%rax), %edx
	movq	-288(%rbp), %rax
	movl	%edx, (%rax)
## BB#3:                                ##   in Loop: Header=BB1_1 Depth=1
	leaq	-296(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB1_1
LBB1_4:
	addq	$200, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi7:
	.cfi_def_cfa_offset 16
Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi9:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi10:
	.cfi_def_cfa_offset 16
Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi12:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_definition	___clang_call_terminate
	.p2align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_definition	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.p2align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi13:
	.cfi_def_cfa_offset 16
Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi15:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.p2align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Lcfi16:
	.cfi_def_cfa_offset 16
Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi18:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	movq	%rdi, %rcx
	addq	%rax, %rcx
	movq	%rcx, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rax
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rdi
Ltmp21:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp22:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB6_1
LBB6_1:
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp23:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp24:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB6_5
LBB6_2:
Ltmp25:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
Ltmp26:
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp27:
	jmp	LBB6_3
LBB6_3:
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB6_4:
Ltmp28:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -120(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB6_5:
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table6:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset16 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp21-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp21
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp21-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset18
Lset19 = Ltmp24-Ltmp21                  ##   Call between Ltmp21 and Ltmp24
	.long	Lset19
Lset20 = Ltmp25-Lfunc_begin1            ##     jumps to Ltmp25
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp26-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset21
Lset22 = Ltmp27-Ltmp26                  ##   Call between Ltmp26 and Ltmp27
	.long	Lset22
Lset23 = Ltmp28-Lfunc_begin1            ##     jumps to Ltmp28
	.long	Lset23
	.byte	1                       ##   On action: 1
Lset24 = Ltmp27-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset24
Lset25 = Lfunc_end1-Ltmp27              ##   Call between Ltmp27 and Lfunc_end1
	.long	Lset25
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi19:
	.cfi_def_cfa_offset 16
Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi21:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.p2align	4, 0x90
__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi22:
	.cfi_def_cfa_offset 16
Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi24:
	.cfi_def_cfa_register %rbp
	subq	$336, %rsp              ## imm = 0x150
	leaq	-312(%rbp), %rax
	leaq	-296(%rbp), %rcx
	movq	%rdi, -304(%rbp)
	movq	-304(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	%rcx, -280(%rbp)
	movq	$-1, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rcx, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	-264(%rbp), %rcx
	movq	$0, (%rcx)
	movq	-296(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	%rax, -184(%rbp)
	cmpq	$0, %rdx
	movq	%rdi, -320(%rbp)        ## 8-byte Spill
	je	LBB8_5
## BB#1:
	movq	-320(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -328(%rbp)        ## 8-byte Spill
LBB8_2:                                 ## =>This Inner Loop Header: Depth=1
	movq	-96(%rbp), %rax
	movq	-328(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB8_4
## BB#3:                                ##   in Loop: Header=BB8_2 Depth=1
	movq	-328(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-4, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	jmp	LBB8_2
LBB8_4:
	movq	-320(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rsi
	movq	%rsi, -168(%rbp)
	movq	-168(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$2, %rdi
	movq	%rcx, -240(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rdi, -256(%rbp)
	movq	-240(%rbp), %rcx
	movq	-248(%rbp), %rdx
	movq	-256(%rbp), %rsi
	movq	%rcx, -200(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rdi
	callq	__ZdlPv
LBB8_5:
	addq	$336, %rsp              ## imm = 0x150
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIKiEEvRT_
	.weak_definition	__ZNSt3__16vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIKiEEvRT_
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIKiEEvRT_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIKiEEvRT_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Lcfi25:
	.cfi_def_cfa_offset 16
Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi27:
	.cfi_def_cfa_register %rbp
	subq	$352, %rsp              ## imm = 0x160
	movq	%rdi, -248(%rbp)
	movq	%rsi, -256(%rbp)
	movq	-248(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -240(%rbp)
	movq	-240(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -232(%rbp)
	movq	-232(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, -264(%rbp)
	movq	%rsi, -216(%rbp)
	movq	-216(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	subq	%rdi, %rax
	sarq	$2, %rax
	addq	$1, %rax
	movq	%rsi, -176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -328(%rbp)        ## 8-byte Spill
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	movq	%rax, -192(%rbp)
	movq	-184(%rbp), %rax
	cmpq	-192(%rbp), %rax
	jbe	LBB9_2
## BB#1:
	movq	-336(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB9_2:
	movq	-336(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	-192(%rbp), %rdx
	shrq	$1, %rdx
	cmpq	%rdx, %rcx
	jb	LBB9_4
## BB#3:
	movq	-192(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	LBB9_8
LBB9_4:
	leaq	-80(%rbp), %rax
	leaq	-184(%rbp), %rcx
	leaq	-208(%rbp), %rdx
	movq	-200(%rbp), %rsi
	shlq	$1, %rsi
	movq	%rsi, -208(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%rcx, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	-72(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB9_6
## BB#5:
	movq	-96(%rbp), %rax
	movq	%rax, -344(%rbp)        ## 8-byte Spill
	jmp	LBB9_7
LBB9_6:
	movq	-88(%rbp), %rax
	movq	%rax, -344(%rbp)        ## 8-byte Spill
LBB9_7:
	movq	-344(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -168(%rbp)
LBB9_8:
	leaq	-304(%rbp), %rdi
	movq	-168(%rbp), %rsi
	movq	-328(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	movq	-264(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC1EmmS3_
	movq	-264(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	-24(%rbp), %rcx
	movl	(%rcx), %r8d
	movl	%r8d, (%rax)
## BB#9:
	movq	-288(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -288(%rbp)
Ltmp29:
	leaq	-304(%rbp), %rsi
	movq	-328(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE
Ltmp30:
	jmp	LBB9_10
LBB9_10:
	leaq	-304(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev
	addq	$352, %rsp              ## imm = 0x160
	popq	%rbp
	retq
LBB9_11:
Ltmp31:
	movl	%edx, %ecx
	movq	%rax, -312(%rbp)
	movl	%ecx, -316(%rbp)
Ltmp32:
	leaq	-304(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev
Ltmp33:
	jmp	LBB9_12
LBB9_12:
	jmp	LBB9_13
LBB9_13:
	movq	-312(%rbp), %rdi
	callq	__Unwind_Resume
LBB9_14:
Ltmp34:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -348(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table9:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset26 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset26
Lset27 = Ltmp29-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp29
	.long	Lset27
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp29-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset28
Lset29 = Ltmp30-Ltmp29                  ##   Call between Ltmp29 and Ltmp30
	.long	Lset29
Lset30 = Ltmp31-Lfunc_begin2            ##     jumps to Ltmp31
	.long	Lset30
	.byte	0                       ##   On action: cleanup
Lset31 = Ltmp30-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset31
Lset32 = Ltmp32-Ltmp30                  ##   Call between Ltmp30 and Ltmp32
	.long	Lset32
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp32-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset33
Lset34 = Ltmp33-Ltmp32                  ##   Call between Ltmp32 and Ltmp33
	.long	Lset34
Lset35 = Ltmp34-Lfunc_begin2            ##     jumps to Ltmp34
	.long	Lset35
	.byte	1                       ##   On action: 1
Lset36 = Ltmp33-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset36
Lset37 = Lfunc_end2-Ltmp33              ##   Call between Ltmp33 and Lfunc_end2
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC1EmmS3_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC1EmmS3_
	.p2align	4, 0x90
__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC1EmmS3_: ## @_ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC1EmmS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi28:
	.cfi_def_cfa_offset 16
Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi30:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE
	.weak_definition	__ZNSt3__16vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi31:
	.cfi_def_cfa_offset 16
Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi33:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$696, %rsp              ## imm = 0x2B8
Lcfi34:
	.cfi_offset %rbx, -24
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rdi, -688(%rbp)
	movq	%rsi, -696(%rbp)
	movq	-688(%rbp), %rsi
	movq	%rsi, -680(%rbp)
	movq	-680(%rbp), %rdi
	movq	%rdi, -672(%rbp)
	movq	-672(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -664(%rbp)
	movq	-664(%rbp), %rdx
	movq	%rdi, -496(%rbp)
	movq	-496(%rbp), %r8
	movq	(%r8), %r8
	movq	%r8, -488(%rbp)
	movq	-488(%rbp), %r8
	movq	%rdi, -536(%rbp)
	movq	-536(%rbp), %r9
	movq	%r9, -528(%rbp)
	movq	-528(%rbp), %r9
	movq	%r9, -520(%rbp)
	movq	-520(%rbp), %r10
	addq	$16, %r10
	movq	%r10, -512(%rbp)
	movq	-512(%rbp), %r10
	movq	%r10, -504(%rbp)
	movq	-504(%rbp), %r10
	movq	(%r10), %r10
	movq	(%r9), %r9
	subq	%r9, %r10
	sarq	$2, %r10
	shlq	$2, %r10
	addq	%r10, %r8
	movq	%rdi, -552(%rbp)
	movq	-552(%rbp), %r9
	movq	(%r9), %r9
	movq	%r9, -544(%rbp)
	movq	-544(%rbp), %r9
	movq	%rdi, -560(%rbp)
	movq	-560(%rbp), %r10
	movq	8(%r10), %r11
	movq	(%r10), %r10
	subq	%r10, %r11
	sarq	$2, %r11
	shlq	$2, %r11
	addq	%r11, %r9
	movq	%rdi, -576(%rbp)
	movq	-576(%rbp), %r10
	movq	(%r10), %r10
	movq	%r10, -568(%rbp)
	movq	-568(%rbp), %r10
	movq	%rdi, -616(%rbp)
	movq	-616(%rbp), %r11
	movq	%r11, -608(%rbp)
	movq	-608(%rbp), %r11
	movq	%r11, -600(%rbp)
	movq	-600(%rbp), %rbx
	addq	$16, %rbx
	movq	%rbx, -592(%rbp)
	movq	-592(%rbp), %rbx
	movq	%rbx, -584(%rbp)
	movq	-584(%rbp), %rbx
	movq	(%rbx), %rbx
	movq	(%r11), %r11
	subq	%r11, %rbx
	sarq	$2, %rbx
	shlq	$2, %rbx
	addq	%rbx, %r10
	movq	%rdi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movq	%r8, -640(%rbp)
	movq	%r9, -648(%rbp)
	movq	%r10, -656(%rbp)
	movq	%rsi, %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	(%rsi), %rdi
	movq	8(%rsi), %r8
	movq	-696(%rbp), %r9
	addq	$8, %r9
	movq	%rdx, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	%r9, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-48(%rbp), %rdi
	subq	%rdi, %rdx
	sarq	$2, %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	(%rdi), %r8
	subq	%rdx, %rcx
	shlq	$2, %rcx
	addq	%rcx, %r8
	movq	%r8, (%rdi)
	cmpq	$0, -72(%rbp)
	movq	%rsi, -704(%rbp)        ## 8-byte Spill
	jle	LBB11_2
## BB#1:
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	-48(%rbp), %rcx
	movq	-72(%rbp), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
LBB11_2:
	leaq	-264(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-120(%rbp), %rdx
	movq	-704(%rbp), %rsi        ## 8-byte Reload
	movq	-696(%rbp), %rdi
	addq	$8, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -120(%rbp)
	movq	-112(%rbp), %rsi
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-104(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-112(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-704(%rbp), %rdx        ## 8-byte Reload
	addq	$8, %rdx
	movq	-696(%rbp), %rsi
	addq	$16, %rsi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-160(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	-704(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	-696(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movq	-216(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-248(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -264(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-248(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movq	-256(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-696(%rbp), %rax
	movq	8(%rax), %rax
	movq	-696(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	movq	%rax, -464(%rbp)
	movq	%rdx, -472(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -448(%rbp)
	movq	-448(%rbp), %rdx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -280(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rcx, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -304(%rbp)
	movq	-304(%rbp), %r8
	movq	%r8, -296(%rbp)
	movq	-296(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdi), %rdi
	subq	%rdi, %r8
	sarq	$2, %r8
	shlq	$2, %r8
	addq	%r8, %rsi
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -336(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %r8
	movq	%r8, -376(%rbp)
	movq	-376(%rbp), %r8
	movq	%r8, -368(%rbp)
	movq	-368(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -360(%rbp)
	movq	-360(%rbp), %r9
	movq	%r9, -352(%rbp)
	movq	-352(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$2, %r9
	shlq	$2, %r9
	addq	%r9, %rdi
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %r8
	movq	(%r8), %r8
	movq	%r8, -392(%rbp)
	movq	-392(%rbp), %r8
	movq	-472(%rbp), %r9
	shlq	$2, %r9
	addq	%r9, %r8
	movq	%rcx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	movq	%rsi, -424(%rbp)
	movq	%rdi, -432(%rbp)
	movq	%r8, -440(%rbp)
	movq	%rax, -480(%rbp)
	addq	$696, %rsp              ## imm = 0x2B8
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev
	.p2align	4, 0x90
__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev: ## @_ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi35:
	.cfi_def_cfa_offset 16
Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi37:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.weak_definition	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.p2align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi38:
	.cfi_def_cfa_offset 16
Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi40:
	.cfi_def_cfa_register %rbp
	subq	$88, %rsp
	leaq	-96(%rbp), %rax
	leaq	-184(%rbp), %rcx
	leaq	-176(%rbp), %rdx
	movabsq	$9223372036854775807, %rsi ## imm = 0x7FFFFFFFFFFFFFFF
	movabsq	$4611686018427387903, %r8 ## imm = 0x3FFFFFFFFFFFFFFF
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rcx, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	-88(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB13_2
## BB#1:
	movq	-112(%rbp), %rax
	movq	%rax, -208(%rbp)        ## 8-byte Spill
	jmp	LBB13_3
LBB13_2:
	movq	-104(%rbp), %rax
	movq	%rax, -208(%rbp)        ## 8-byte Spill
LBB13_3:
	movq	-208(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-216(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$88, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_
	.p2align	4, 0x90
__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_: ## @_ZNSt3__114__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Lcfi41:
	.cfi_def_cfa_offset 16
Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi43:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	leaq	-64(%rbp), %rax
	leaq	-96(%rbp), %r8
	leaq	-400(%rbp), %r9
	leaq	-360(%rbp), %r10
	movq	%rdi, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rcx, -392(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	%r10, -344(%rbp)
	movq	$-1, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdi
	movq	%rsi, -328(%rbp)
	movq	%rdi, -336(%rbp)
	movq	-328(%rbp), %rsi
	movq	$0, (%rsi)
	movq	-360(%rbp), %rsi
	movq	%rsi, -400(%rbp)
	movq	%r9, -136(%rbp)
	movq	-392(%rbp), %rsi
	movq	%rdx, -112(%rbp)
	movq	$0, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-88(%rbp), %rdx
	movq	%r8, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-104(%rbp), %rdi
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-72(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, 8(%rdx)
	cmpq	$0, -376(%rbp)
	movq	%rcx, -416(%rbp)        ## 8-byte Spill
	je	LBB14_6
## BB#1:
	movabsq	$4611686018427387903, %rax ## imm = 0x3FFFFFFFFFFFFFFF
	movq	-416(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	8(%rdx), %rdx
	movq	-376(%rbp), %rsi
	movq	%rdx, -240(%rbp)
	movq	%rsi, -248(%rbp)
	movq	-240(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	%rdx, -216(%rbp)
	movq	%rsi, -224(%rbp)
	movq	$0, -232(%rbp)
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%rdx, -208(%rbp)
	cmpq	%rax, %rsi
	jbe	LBB14_5
## BB#2:
	leaq	L_.str.1(%rip), %rax
	movq	%rax, -176(%rbp)
	movl	$16, %ecx
	movl	%ecx, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	-176(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rsi
Ltmp35:
	movq	%rdi, -424(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rax, -432(%rbp)        ## 8-byte Spill
	callq	__ZNSt11logic_errorC2EPKc
Ltmp36:
	jmp	LBB14_3
LBB14_3:
	movq	__ZTISt12length_error@GOTPCREL(%rip), %rax
	movq	__ZNSt12length_errorD1Ev@GOTPCREL(%rip), %rcx
	movq	__ZTVSt12length_error@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	-432(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, (%rsi)
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
LBB14_4:
Ltmp37:
	movl	%edx, %ecx
	movq	%rax, -184(%rbp)
	movl	%ecx, -188(%rbp)
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
	movq	-184(%rbp), %rdi
	callq	__Unwind_Resume
LBB14_5:
	movq	-224(%rbp), %rax
	shlq	$2, %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -440(%rbp)        ## 8-byte Spill
	jmp	LBB14_7
LBB14_6:
	xorl	%eax, %eax
	movl	%eax, %ecx
	leaq	-408(%rbp), %rdx
	leaq	-288(%rbp), %rsi
	movq	%rsi, -272(%rbp)
	movq	$-1, -280(%rbp)
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %rdi
	movq	%rsi, -256(%rbp)
	movq	%rdi, -264(%rbp)
	movq	-256(%rbp), %rsi
	movq	$0, (%rsi)
	movq	-288(%rbp), %rsi
	movq	%rsi, -408(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
LBB14_7:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	-416(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-384(%rbp), %rdx
	shlq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	-376(%rbp), %rdx
	shlq	$2, %rdx
	addq	%rdx, %rax
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -312(%rbp)
	movq	-312(%rbp), %rdx
	movq	%rdx, -304(%rbp)
	movq	-304(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table14:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset38 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset38
Lset39 = Ltmp35-Lfunc_begin3            ##   Call between Lfunc_begin3 and Ltmp35
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp35-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset40
Lset41 = Ltmp36-Ltmp35                  ##   Call between Ltmp35 and Ltmp36
	.long	Lset41
Lset42 = Ltmp37-Lfunc_begin3            ##     jumps to Ltmp37
	.long	Lset42
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp36-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset43
Lset44 = Lfunc_end3-Ltmp36              ##   Call between Ltmp36 and Lfunc_end3
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED2Ev
	.p2align	4, 0x90
__ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED2Ev: ## @_ZNSt3__114__split_bufferIiRNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi44:
	.cfi_def_cfa_offset 16
Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi46:
	.cfi_def_cfa_register %rbp
	subq	$304, %rsp              ## imm = 0x130
	movq	%rdi, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	%rdi, -296(%rbp)        ## 8-byte Spill
	movq	%rax, -304(%rbp)        ## 8-byte Spill
LBB15_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-304(%rbp), %rcx        ## 8-byte Reload
	cmpq	16(%rcx), %rax
	je	LBB15_3
## BB#2:                                ##   in Loop: Header=BB15_1 Depth=1
	movq	-304(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rdx
	addq	$-4, %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	jmp	LBB15_1
LBB15_3:
	movq	-296(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, (%rax)
	je	LBB15_5
## BB#4:
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$2, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__ZdlPv
LBB15_5:
	addq	$304, %rsp              ## imm = 0x130
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Lcfi47:
	.cfi_def_cfa_offset 16
Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi49:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp              ## imm = 0x1D8
Lcfi50:
	.cfi_offset %rbx, -56
Lcfi51:
	.cfi_offset %r12, -48
Lcfi52:
	.cfi_offset %r13, -40
Lcfi53:
	.cfi_offset %r14, -32
Lcfi54:
	.cfi_offset %r15, -24
	leaq	-240(%rbp), %rax
	leaq	-224(%rbp), %rcx
	leaq	-256(%rbp), %rdx
	leaq	-272(%rbp), %r8
	leaq	-352(%rbp), %r9
	leaq	-184(%rbp), %r10
	leaq	-344(%rbp), %r11
	leaq	-136(%rbp), %rbx
	leaq	-336(%rbp), %r14
	leaq	-88(%rbp), %r15
	leaq	-456(%rbp), %r12
	movq	%rdi, -440(%rbp)
	movq	%rsi, -448(%rbp)
	movq	-440(%rbp), %rsi
	movq	%rsi, %rdi
	movq	-448(%rbp), %r13
	movq	%r13, -432(%rbp)
	movq	-432(%rbp), %r13
	addq	$16, %r13
	movq	%r13, -424(%rbp)
	movq	-424(%rbp), %r13
	movq	%r13, -416(%rbp)
	movq	-416(%rbp), %r13
	movq	%r13, -384(%rbp)
	movq	-384(%rbp), %r13
	movq	%r13, -368(%rbp)
	movq	%rdi, -320(%rbp)
	movq	%r12, -328(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, %r12
	movq	%r12, -312(%rbp)
	movq	%r15, -72(%rbp)
	movq	$-1, -80(%rbp)
	movq	-72(%rbp), %r15
	movq	-80(%rbp), %r12
	movq	%r15, -56(%rbp)
	movq	%r12, -64(%rbp)
	movq	-56(%rbp), %r15
	movq	$0, (%r15)
	movq	-88(%rbp), %r15
	movq	%r15, -336(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, (%rdi)
	movq	%rbx, -120(%rbp)
	movq	$-1, -128(%rbp)
	movq	-120(%rbp), %rbx
	movq	-128(%rbp), %r14
	movq	%rbx, -104(%rbp)
	movq	%r14, -112(%rbp)
	movq	-104(%rbp), %rbx
	movq	$0, (%rbx)
	movq	-136(%rbp), %rbx
	movq	%rbx, -344(%rbp)
	movq	%r11, -144(%rbp)
	movq	$0, 8(%rdi)
	addq	$16, %rdi
	movq	%r10, -168(%rbp)
	movq	$-1, -176(%rbp)
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r11
	movq	%r10, -152(%rbp)
	movq	%r11, -160(%rbp)
	movq	-152(%rbp), %r10
	movq	$0, (%r10)
	movq	-184(%rbp), %r10
	movq	%r10, -352(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rdi, -296(%rbp)
	movq	$0, -304(%rbp)
	movq	-296(%rbp), %rdi
	movq	-304(%rbp), %r9
	movq	%rdi, -264(%rbp)
	movq	%r9, -272(%rbp)
	movq	-264(%rbp), %rdi
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	(%r8), %r8
	movq	%rdx, -200(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%r8, -240(%rbp)
	movq	-232(%rbp), %rdx
	movq	%rcx, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-448(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	subq	%rax, %rcx
	sarq	$2, %rcx
	movq	%rcx, -472(%rbp)
	cmpq	$0, -472(%rbp)
	movq	%rsi, -496(%rbp)        ## 8-byte Spill
	jbe	LBB16_5
## BB#1:
	movq	-472(%rbp), %rsi
Ltmp38:
	movq	-496(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
Ltmp39:
	jmp	LBB16_2
LBB16_2:
	movq	-448(%rbp), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	-472(%rbp), %rcx
Ltmp40:
	movq	-496(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
Ltmp41:
	jmp	LBB16_3
LBB16_3:
	jmp	LBB16_5
LBB16_4:
Ltmp42:
	movl	%edx, %ecx
	movq	%rax, -480(%rbp)
	movl	%ecx, -484(%rbp)
Ltmp43:
	movq	-496(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
Ltmp44:
	jmp	LBB16_6
LBB16_5:
	addq	$472, %rsp              ## imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB16_6:
	jmp	LBB16_7
LBB16_7:
	movq	-480(%rbp), %rdi
	callq	__Unwind_Resume
LBB16_8:
Ltmp45:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -500(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table16:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\257\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset45 = Ltmp38-Lfunc_begin4            ## >> Call Site 1 <<
	.long	Lset45
Lset46 = Ltmp41-Ltmp38                  ##   Call between Ltmp38 and Ltmp41
	.long	Lset46
Lset47 = Ltmp42-Lfunc_begin4            ##     jumps to Ltmp42
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp43-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset48
Lset49 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset49
Lset50 = Ltmp45-Lfunc_begin4            ##     jumps to Ltmp45
	.long	Lset50
	.byte	1                       ##   On action: 1
Lset51 = Ltmp44-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset51
Lset52 = Lfunc_end4-Ltmp44              ##   Call between Ltmp44 and Lfunc_end4
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.weak_definition	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Lcfi55:
	.cfi_def_cfa_offset 16
Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi57:
	.cfi_def_cfa_register %rbp
	subq	$416, %rsp              ## imm = 0x1A0
	movq	%rdi, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	-368(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%rdi, -384(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rsi, -392(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	movq	-384(%rbp), %rsi        ## 8-byte Reload
	cmpq	%rax, %rsi
	jbe	LBB17_2
## BB#1:
	movq	-392(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB17_2:
	movabsq	$4611686018427387903, %rax ## imm = 0x3FFFFFFFFFFFFFFF
	movq	-392(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rcx, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	$0, -96(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -72(%rbp)
	cmpq	%rax, %rdx
	jbe	LBB17_6
## BB#3:
	leaq	L_.str.1(%rip), %rax
	movq	%rax, -40(%rbp)
	movl	$16, %ecx
	movl	%ecx, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	-40(%rbp), %rdx
	movq	%rax, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rsi
Ltmp46:
	movq	%rdi, -400(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)        ## 8-byte Spill
	callq	__ZNSt11logic_errorC2EPKc
Ltmp47:
	jmp	LBB17_4
LBB17_4:
	movq	__ZTISt12length_error@GOTPCREL(%rip), %rax
	movq	__ZNSt12length_errorD1Ev@GOTPCREL(%rip), %rcx
	movq	__ZTVSt12length_error@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, (%rsi)
	movq	-400(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
LBB17_5:
Ltmp48:
	movl	%edx, %ecx
	movq	%rax, -48(%rbp)
	movl	%ecx, -52(%rbp)
	movq	-400(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
	movq	-48(%rbp), %rdi
	callq	__Unwind_Resume
LBB17_6:
	movq	-88(%rbp), %rax
	shlq	$2, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__Znwm
	movq	-392(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rdi), %rax
	movq	-376(%rbp), %rcx
	shlq	$2, %rcx
	addq	%rcx, %rax
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-392(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	$0, -336(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -168(%rbp)
	movq	-168(%rbp), %r8
	movq	%r8, -160(%rbp)
	movq	-160(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rsi), %rsi
	subq	%rsi, %r8
	sarq	$2, %r8
	shlq	$2, %r8
	addq	%r8, %rdx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -200(%rbp)
	movq	-200(%rbp), %rsi
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -240(%rbp)
	movq	-240(%rbp), %r8
	movq	%r8, -232(%rbp)
	movq	-232(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -224(%rbp)
	movq	-224(%rbp), %r9
	movq	%r9, -216(%rbp)
	movq	-216(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$2, %r9
	shlq	$2, %r9
	addq	%r9, %rsi
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %r8
	movq	(%r8), %r8
	movq	%r8, -256(%rbp)
	movq	-256(%rbp), %r8
	movq	-336(%rbp), %r9
	shlq	$2, %r9
	addq	%r9, %r8
	movq	%rcx, -272(%rbp)
	movq	%rdi, -280(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%r8, -304(%rbp)
	addq	$416, %rsp              ## imm = 0x1A0
	popq	%rbp
	retq
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table17:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset53 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp46-Lfunc_begin5            ##   Call between Lfunc_begin5 and Ltmp46
	.long	Lset54
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset55 = Ltmp46-Lfunc_begin5            ## >> Call Site 2 <<
	.long	Lset55
Lset56 = Ltmp47-Ltmp46                  ##   Call between Ltmp46 and Ltmp47
	.long	Lset56
Lset57 = Ltmp48-Lfunc_begin5            ##     jumps to Ltmp48
	.long	Lset57
	.byte	0                       ##   On action: cleanup
Lset58 = Ltmp47-Lfunc_begin5            ## >> Call Site 3 <<
	.long	Lset58
Lset59 = Lfunc_end5-Ltmp47              ##   Call between Ltmp47 and Lfunc_end5
	.long	Lset59
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.weak_definition	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.p2align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi58:
	.cfi_def_cfa_offset 16
Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi60:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	leaq	-168(%rbp), %rax
	movq	%rdi, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -120(%rbp)
	movq	-120(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	-48(%rbp), %rsi
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-160(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rsi
	addq	$8, %rcx
	movq	%rax, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rcx
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, -88(%rbp)
	cmpq	$0, -88(%rbp)
	jle	LBB18_2
## BB#1:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	-64(%rbp), %rcx
	movq	-88(%rbp), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rcx
	shlq	$2, %rax
	addq	(%rcx), %rax
	movq	%rax, (%rcx)
LBB18_2:
	leaq	-168(%rbp), %rax
	movq	%rax, -96(%rbp)
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_definition	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.p2align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Lcfi61:
	.cfi_def_cfa_offset 16
Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi63:
	.cfi_def_cfa_register %rbp
	subq	$416, %rsp              ## imm = 0x1A0
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-200(%rbp), %rsi
Ltmp49:
	leaq	-232(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp50:
	jmp	LBB19_1
LBB19_1:
	leaq	-232(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -265(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-265(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB19_3
	jmp	LBB19_26
LBB19_3:
	leaq	-256(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	%rax, -176(%rbp)
	movq	%rcx, -184(%rbp)
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -280(%rbp)        ## 8-byte Spill
	movl	%edi, -284(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-284(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB19_6
## BB#5:
	movq	-208(%rbp), %rax
	addq	-216(%rbp), %rax
	movq	%rax, -296(%rbp)        ## 8-byte Spill
	jmp	LBB19_7
LBB19_6:
	movq	-208(%rbp), %rax
	movq	%rax, -296(%rbp)        ## 8-byte Spill
LBB19_7:
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	-208(%rbp), %rcx
	addq	-216(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-200(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -304(%rbp)        ## 8-byte Spill
	movq	%rcx, -312(%rbp)        ## 8-byte Spill
	movq	%rdx, -320(%rbp)        ## 8-byte Spill
	movq	%rsi, -328(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-328(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB19_8
	jmp	LBB19_16
LBB19_8:
	movq	-328(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp51:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp52:
	jmp	LBB19_9
LBB19_9:
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rdi
Ltmp53:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp54:
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	jmp	LBB19_10
LBB19_10:
	movb	-33(%rbp), %al
	movq	-336(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp55:
	movl	%edi, -340(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-340(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -352(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-352(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp56:
	movb	%al, -353(%rbp)         ## 1-byte Spill
	jmp	LBB19_14
LBB19_11:
Ltmp57:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
Ltmp58:
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp59:
	jmp	LBB19_12
LBB19_12:
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	movl	%ecx, -372(%rbp)        ## 4-byte Spill
	jmp	LBB19_24
LBB19_13:
Ltmp60:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -376(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB19_14:
Ltmp61:
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp62:
	jmp	LBB19_15
LBB19_15:
	movb	-353(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-328(%rbp), %rdx        ## 8-byte Reload
	movl	%ecx, 144(%rdx)
LBB19_16:
	movq	-328(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -377(%rbp)         ## 1-byte Spill
## BB#17:
	movq	-256(%rbp), %rdi
Ltmp63:
	movb	-377(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-280(%rbp), %rsi        ## 8-byte Reload
	movq	-304(%rbp), %rdx        ## 8-byte Reload
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	-320(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp64:
	movq	%rax, -392(%rbp)        ## 8-byte Spill
	jmp	LBB19_18
LBB19_18:
	leaq	-264(%rbp), %rax
	movq	-392(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -264(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB19_25
## BB#19:
	movq	-200(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movl	-116(%rbp), %edx
	movq	%rax, -96(%rbp)
	movl	%edx, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	movl	-100(%rbp), %esi
	orl	%esi, %edx
Ltmp65:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp66:
	jmp	LBB19_20
LBB19_20:
	jmp	LBB19_21
LBB19_21:
	jmp	LBB19_25
LBB19_22:
Ltmp72:
	movl	%edx, %ecx
	movq	%rax, -240(%rbp)
	movl	%ecx, -244(%rbp)
	jmp	LBB19_29
LBB19_23:
Ltmp67:
	movl	%edx, %ecx
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	movl	%ecx, -372(%rbp)        ## 4-byte Spill
	jmp	LBB19_24
LBB19_24:
	movl	-372(%rbp), %eax        ## 4-byte Reload
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movl	%eax, -244(%rbp)
Ltmp68:
	leaq	-232(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
Ltmp69:
	jmp	LBB19_28
LBB19_25:
	jmp	LBB19_26
LBB19_26:
Ltmp70:
	leaq	-232(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
Ltmp71:
	jmp	LBB19_27
LBB19_27:
	jmp	LBB19_31
LBB19_28:
	jmp	LBB19_29
LBB19_29:
	movq	-240(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-200(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp73:
	movq	%rax, -400(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp74:
	jmp	LBB19_30
LBB19_30:
	callq	___cxa_end_catch
LBB19_31:
	movq	-200(%rbp), %rax
	addq	$416, %rsp              ## imm = 0x1A0
	popq	%rbp
	retq
LBB19_32:
Ltmp75:
	movl	%edx, %ecx
	movq	%rax, -240(%rbp)
	movl	%ecx, -244(%rbp)
Ltmp76:
	callq	___cxa_end_catch
Ltmp77:
	jmp	LBB19_33
LBB19_33:
	jmp	LBB19_34
LBB19_34:
	movq	-240(%rbp), %rdi
	callq	__Unwind_Resume
LBB19_35:
Ltmp78:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -404(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table19:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\253\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\234\001"              ## Call site table length
Lset60 = Ltmp49-Lfunc_begin6            ## >> Call Site 1 <<
	.long	Lset60
Lset61 = Ltmp50-Ltmp49                  ##   Call between Ltmp49 and Ltmp50
	.long	Lset61
Lset62 = Ltmp72-Lfunc_begin6            ##     jumps to Ltmp72
	.long	Lset62
	.byte	5                       ##   On action: 3
Lset63 = Ltmp51-Lfunc_begin6            ## >> Call Site 2 <<
	.long	Lset63
Lset64 = Ltmp52-Ltmp51                  ##   Call between Ltmp51 and Ltmp52
	.long	Lset64
Lset65 = Ltmp67-Lfunc_begin6            ##     jumps to Ltmp67
	.long	Lset65
	.byte	5                       ##   On action: 3
Lset66 = Ltmp53-Lfunc_begin6            ## >> Call Site 3 <<
	.long	Lset66
Lset67 = Ltmp56-Ltmp53                  ##   Call between Ltmp53 and Ltmp56
	.long	Lset67
Lset68 = Ltmp57-Lfunc_begin6            ##     jumps to Ltmp57
	.long	Lset68
	.byte	3                       ##   On action: 2
Lset69 = Ltmp58-Lfunc_begin6            ## >> Call Site 4 <<
	.long	Lset69
Lset70 = Ltmp59-Ltmp58                  ##   Call between Ltmp58 and Ltmp59
	.long	Lset70
Lset71 = Ltmp60-Lfunc_begin6            ##     jumps to Ltmp60
	.long	Lset71
	.byte	7                       ##   On action: 4
Lset72 = Ltmp61-Lfunc_begin6            ## >> Call Site 5 <<
	.long	Lset72
Lset73 = Ltmp66-Ltmp61                  ##   Call between Ltmp61 and Ltmp66
	.long	Lset73
Lset74 = Ltmp67-Lfunc_begin6            ##     jumps to Ltmp67
	.long	Lset74
	.byte	5                       ##   On action: 3
Lset75 = Ltmp68-Lfunc_begin6            ## >> Call Site 6 <<
	.long	Lset75
Lset76 = Ltmp69-Ltmp68                  ##   Call between Ltmp68 and Ltmp69
	.long	Lset76
Lset77 = Ltmp78-Lfunc_begin6            ##     jumps to Ltmp78
	.long	Lset77
	.byte	5                       ##   On action: 3
Lset78 = Ltmp70-Lfunc_begin6            ## >> Call Site 7 <<
	.long	Lset78
Lset79 = Ltmp71-Ltmp70                  ##   Call between Ltmp70 and Ltmp71
	.long	Lset79
Lset80 = Ltmp72-Lfunc_begin6            ##     jumps to Ltmp72
	.long	Lset80
	.byte	5                       ##   On action: 3
Lset81 = Ltmp71-Lfunc_begin6            ## >> Call Site 8 <<
	.long	Lset81
Lset82 = Ltmp73-Ltmp71                  ##   Call between Ltmp71 and Ltmp73
	.long	Lset82
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset83 = Ltmp73-Lfunc_begin6            ## >> Call Site 9 <<
	.long	Lset83
Lset84 = Ltmp74-Ltmp73                  ##   Call between Ltmp73 and Ltmp74
	.long	Lset84
Lset85 = Ltmp75-Lfunc_begin6            ##     jumps to Ltmp75
	.long	Lset85
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp74-Lfunc_begin6            ## >> Call Site 10 <<
	.long	Lset86
Lset87 = Ltmp76-Ltmp74                  ##   Call between Ltmp74 and Ltmp76
	.long	Lset87
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset88 = Ltmp76-Lfunc_begin6            ## >> Call Site 11 <<
	.long	Lset88
Lset89 = Ltmp77-Ltmp76                  ##   Call between Ltmp76 and Ltmp77
	.long	Lset89
Lset90 = Ltmp78-Lfunc_begin6            ##     jumps to Ltmp78
	.long	Lset90
	.byte	5                       ##   On action: 3
Lset91 = Ltmp77-Lfunc_begin6            ## >> Call Site 12 <<
	.long	Lset91
Lset92 = Lfunc_end6-Ltmp77              ##   Call between Ltmp77 and Lfunc_end6
	.long	Lset92
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 4 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 3
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_definition	__ZNSt3__111char_traitsIcE6lengthEPKc
	.p2align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi64:
	.cfi_def_cfa_offset 16
Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi66:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_definition	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.p2align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Lcfi67:
	.cfi_def_cfa_offset 16
Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi69:
	.cfi_def_cfa_register %rbp
	subq	$720, %rsp              ## imm = 0x2D0
	movb	%r9b, %al
	leaq	-552(%rbp), %r10
	leaq	-488(%rbp), %r11
	movq	%rdi, -504(%rbp)
	movq	%rsi, -512(%rbp)
	movq	%rdx, -520(%rbp)
	movq	%rcx, -528(%rbp)
	movq	%r8, -536(%rbp)
	movb	%al, -537(%rbp)
	movq	-504(%rbp), %rcx
	movq	%r11, -472(%rbp)
	movq	$-1, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rdx, -456(%rbp)
	movq	%rsi, -464(%rbp)
	movq	-456(%rbp), %rdx
	movq	$0, (%rdx)
	movq	-488(%rbp), %rdx
	movq	%rdx, -552(%rbp)
	movq	%r10, -448(%rbp)
	cmpq	$0, %rcx
	jne	LBB21_2
## BB#1:
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	jmp	LBB21_29
LBB21_2:
	movq	-528(%rbp), %rax
	movq	-512(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -560(%rbp)
	movq	-536(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -568(%rbp)
	movq	-568(%rbp), %rax
	cmpq	-560(%rbp), %rax
	jle	LBB21_4
## BB#3:
	movq	-560(%rbp), %rax
	movq	-568(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -568(%rbp)
	jmp	LBB21_5
LBB21_4:
	movq	$0, -568(%rbp)
LBB21_5:
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -576(%rbp)
	cmpq	$0, -576(%rbp)
	jle	LBB21_9
## BB#6:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %rcx
	movq	-576(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-576(%rbp), %rax
	je	LBB21_8
## BB#7:
	leaq	-584(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	$-1, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	-232(%rbp), %rdx
	movq	%rcx, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-208(%rbp), %rcx
	movq	$0, (%rcx)
	movq	-240(%rbp), %rcx
	movq	%rcx, -584(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	jmp	LBB21_29
LBB21_8:
	jmp	LBB21_9
LBB21_9:
	cmpq	$0, -568(%rbp)
	jle	LBB21_24
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-608(%rbp), %rcx
	movq	-568(%rbp), %rdi
	movb	-537(%rbp), %r8b
	movq	%rcx, -72(%rbp)
	movq	%rdi, -80(%rbp)
	movb	%r8b, -81(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movb	-81(%rbp), %r8b
	movq	%rcx, -48(%rbp)
	movq	%rdi, -56(%rbp)
	movb	%r8b, -57(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -16(%rbp)
	movq	%rcx, -656(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-56(%rbp), %rsi
	movq	-656(%rbp), %rdi        ## 8-byte Reload
	movsbl	-57(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-608(%rbp), %rcx
	movq	-504(%rbp), %rsi
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rdi
	movq	%rdi, -176(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -664(%rbp)        ## 8-byte Spill
	movq	%rcx, -672(%rbp)        ## 8-byte Spill
	je	LBB21_12
## BB#11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -680(%rbp)        ## 8-byte Spill
	jmp	LBB21_13
LBB21_12:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -680(%rbp)        ## 8-byte Spill
LBB21_13:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	-568(%rbp), %rcx
	movq	-664(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	-272(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-280(%rbp), %rsi
	movq	-288(%rbp), %rdx
Ltmp79:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp80:
	movq	%rax, -688(%rbp)        ## 8-byte Spill
	jmp	LBB21_14
LBB21_14:
	jmp	LBB21_15
LBB21_15:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	cmpq	-568(%rbp), %rax
	je	LBB21_20
## BB#16:
	leaq	-328(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	$-1, -320(%rbp)
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	%rax, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	-296(%rbp), %rax
	movq	$0, (%rax)
	movq	-328(%rbp), %rax
	movq	%rax, -696(%rbp)        ## 8-byte Spill
## BB#17:
	leaq	-632(%rbp), %rax
	movq	-696(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -632(%rbp)
	movq	%rax, -336(%rbp)
## BB#18:
	movq	$0, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	movl	$1, -636(%rbp)
	jmp	LBB21_21
LBB21_19:
Ltmp81:
	movl	%edx, %ecx
	movq	%rax, -616(%rbp)
	movl	%ecx, -620(%rbp)
Ltmp82:
	leaq	-608(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp83:
	jmp	LBB21_23
LBB21_20:
	movl	$0, -636(%rbp)
LBB21_21:
	leaq	-608(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -700(%rbp)        ## 4-byte Spill
	je	LBB21_22
	jmp	LBB21_33
LBB21_33:
	movl	-700(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -704(%rbp)        ## 4-byte Spill
	je	LBB21_29
	jmp	LBB21_32
LBB21_22:
	jmp	LBB21_24
LBB21_23:
	jmp	LBB21_30
LBB21_24:
	movq	-528(%rbp), %rax
	movq	-520(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -576(%rbp)
	cmpq	$0, -576(%rbp)
	jle	LBB21_28
## BB#25:
	movq	-504(%rbp), %rax
	movq	-520(%rbp), %rcx
	movq	-576(%rbp), %rdx
	movq	%rax, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-360(%rbp), %rsi
	movq	-368(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-576(%rbp), %rax
	je	LBB21_27
## BB#26:
	leaq	-648(%rbp), %rax
	leaq	-408(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	movq	$-1, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	$0, (%rcx)
	movq	-408(%rbp), %rcx
	movq	%rcx, -648(%rbp)
	movq	%rax, -416(%rbp)
	movq	$0, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	jmp	LBB21_29
LBB21_27:
	jmp	LBB21_28
LBB21_28:
	movq	-536(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	$0, -432(%rbp)
	movq	-424(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -440(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
LBB21_29:
	movq	-496(%rbp), %rax
	addq	$720, %rsp              ## imm = 0x2D0
	popq	%rbp
	retq
LBB21_30:
	movq	-616(%rbp), %rdi
	callq	__Unwind_Resume
LBB21_31:
Ltmp84:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -708(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB21_32:
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.p2align	2
GCC_except_table21:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset93 = Lfunc_begin7-Lfunc_begin7      ## >> Call Site 1 <<
	.long	Lset93
Lset94 = Ltmp79-Lfunc_begin7            ##   Call between Lfunc_begin7 and Ltmp79
	.long	Lset94
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset95 = Ltmp79-Lfunc_begin7            ## >> Call Site 2 <<
	.long	Lset95
Lset96 = Ltmp80-Ltmp79                  ##   Call between Ltmp79 and Ltmp80
	.long	Lset96
Lset97 = Ltmp81-Lfunc_begin7            ##     jumps to Ltmp81
	.long	Lset97
	.byte	0                       ##   On action: cleanup
Lset98 = Ltmp82-Lfunc_begin7            ## >> Call Site 3 <<
	.long	Lset98
Lset99 = Ltmp83-Ltmp82                  ##   Call between Ltmp82 and Ltmp83
	.long	Lset99
Lset100 = Ltmp84-Lfunc_begin7           ##     jumps to Ltmp84
	.long	Lset100
	.byte	1                       ##   On action: 1
Lset101 = Ltmp83-Lfunc_begin7           ## >> Call Site 4 <<
	.long	Lset101
Lset102 = Lfunc_end7-Ltmp83             ##   Call between Ltmp83 and Lfunc_end7
	.long	Lset102
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.p2align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_definition	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.p2align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi70:
	.cfi_def_cfa_offset 16
Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi72:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_definition	__ZNSt3__111char_traitsIcE3eofEv
	.p2align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Lcfi73:
	.cfi_def_cfa_offset 16
Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Lcfi75:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"sum is "

L_.str.1:                               ## @.str.1
	.asciz	"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size"


.subsections_via_symbols

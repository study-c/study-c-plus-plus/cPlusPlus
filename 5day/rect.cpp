#include <iostream>
using namespace std;

class Rectangle {
    int width, height;
public:
    Rectangle();
    Rectangle(int, int);
    ~Rectangle();
    void set_values (int,int);
    int area() {return width*height;}
};

Rectangle::Rectangle(): width(0), height(0){
    cout << "Empty Created" << endl;
}
Rectangle::~Rectangle() {
    cout << "Deleted" << endl;
}
Rectangle::Rectangle(int w, int h): width(w), height(h){
    cout << "Created" << endl;
}
void Rectangle::set_values (int x, int y) {
  width = x;
  height = y;
}

int main () {
  Rectangle *rect = new Rectangle(2,5);
//   rect.set_values (3,4);
  cout << "area: " << rect->area() << endl;
  delete rect;
  return 0;
}
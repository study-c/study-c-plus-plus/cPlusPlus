#include <iostream>
#include <string>
using namespace std;

template <class T>
T sum(const T arr[], size_t size, T s = 0) {
    for(size_t i = 0; i < size; i++) {
        s += arr[i];
    }
    return s;
}

int main()
{
    int arr[] = {1,2,3,4,5,6};

    cout << "Sum of arr "<< sum(arr,6) << endl;
    cout << "Sum of arr "<< sum(arr,6, 10) << endl;
    return 0;
}

#include <iostream>
using namespace std;

class Node {
    int data;
    Node *next;

public:
    Node(): data(0), next(NULL){};
    void setData(int value) { data = value };
    int getData() { return data };
    void setNext(const Node& n) { next = n };
    Node* getNext() { return next }
}

class List {
    Node *head;
public:

}
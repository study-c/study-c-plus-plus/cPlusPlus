#include <iostream>

using namespace std;

class Person
{
	public:
		Person(int age);
		virtual void print();
	private:
		int age;
};

class Male : public Person
{
	public:
		Male( int m_age ) : Person( m_age ) {}
};

Person::Person( int age )
{
	this -> age = age;
}

void Person::print()
{
	cout << this->age << endl;
}

int main ( int argc, char **argv )
{
	Male m(5);

	m.print();
}

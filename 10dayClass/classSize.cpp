#include <iostream>

using namespace std;

class A 
{
	public:
		int i;
		int j;
		int k;
};

class B 
{
	public:
		int i;
		int j;
		int k;
		static int m;
		B();
		~B();
};

struct D 
{
	public:
		int i;
		int j;
		int k;
};
struct E 
{
	public:
		int i;
		int j;
		int k;
		static int m;
};

int main( int argc, char **argv )
{

	cout << sizeof( A ) << endl;
	cout << sizeof( B ) << endl;
	cout << sizeof( D ) << endl;
	cout << sizeof( E ) << endl;
	return 0;
}

#include <iostream>

using namespace std;

template <typename T>
class Test
{
	friend ostream & operator << ( ostream &out, Test &t )
	{
		out << t.a << endl;
		return out;
	}
	public:
		Test( T a)
		{
			this -> a = a;
		}

		T getValue()
		{
			return a;
		}

	private:
		T a;
};

int main( int argc, char ** argv )
{
	Test<int> t1(3), t2(5);

	cout << t1 << t2 << endl;
	return 0;
}


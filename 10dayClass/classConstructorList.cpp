#include <iostream>
#include <string>

using namespace std;

class Person 
{
    public:
        Person ();
        Person( int age, const string &name);
        Person( const Person &personToCopy );
        int getAge();
        string getName();
    private:
        int age;
        string name;
};

Person:: Person()
{
    age = 31;
    name = "Jerry";
}

Person:: Person( int age, const string &name )
{
    this->age = age;
    this->name = name;
}

Person :: Person( const Person &personToCopy )
{
    age = personToCopy.age;
    name = personToCopy.name;
}

int Person :: getAge()
{
    return age;
}

string Person :: getName()
{
    return name;
}

class ClassRoom
{
    public:
        ClassRoom() : s1() {};
        ClassRoom( int age, const string &name ) : s1( age, name ){}; 
        int getStudentAge() { return s1.getAge(); };
        string getStudentName() { return s1.getName(); };

    private:
        Person s1;
};

int main( int argc, char **argv )
{
    Person p1;
    Person p2(3, "Tom");

    cout << "age: " << p1.getAge() << " name: " << p1.getName() << endl;
    cout << "age: " << p2.getAge() << " name: " << p2.getName() << endl;

    ClassRoom r1;
    ClassRoom r2(33, "pp");

    cout << "age: " << r1.getStudentAge() << " name: " << r1.getStudentName() << endl;
    cout << "age: " << r2.getStudentAge() << " name: " << r2.getStudentName() << endl;
    return 0;
}
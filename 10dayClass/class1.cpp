#include <iostream>
#include <string>

using namespace std;

class Circle
{
	public:
		double r;
		double s;
		Circle( double r );
		Circle();
		double getArea();
		~Circle();
};

Circle::Circle()
{
	cout << "Class Circle is contructed" << endl;
	this->r = 3.0; 
}


Circle::Circle( double r )
{
	this->r = r;
}

double Circle::getArea()
{
	return 3.14 * this->r * this->r;
}

Circle::~Circle()
{
	cout << "Class Circle is destructed" << endl;
}

void print1( const Circle &c )
{
	cout << c.r << endl;
}
int main( int argc, char ** argv)
{
	Circle *c1 = new Circle( 1.4 );
	Circle c2;

	Circle c3(1.6);
	Circle c4 = (1.7);
	Circle c5 = Circle(2.5);
	cout << c1 -> getArea() << endl;
	cout << c3.getArea() << endl;
	cout << c4.getArea() << endl;
	cout << c5.getArea() << endl;

	print1( *c1 );
	delete c1;
	return 0;
}

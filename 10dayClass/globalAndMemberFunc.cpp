#include <iostream>

using namespace std;

class A 
{
	public:
		A();
		A( const A &a);
		~A();
		A(int age);
		int getAge();
	private:
		int age;
		friend void changeAge( A *ptr, int age );
};

void changeAge( A *ptr, int age )
{
	ptr -> age = age;
}

A::A()
{
	age = 0;
}
A::A( const A &a)
{
	age = a.age;
}
A :: A(int age)
{
	this->age = age;
}

A :: ~A()
{
	cout << "A was destroyed" << endl;
}

int A :: getAge()
{
	return age;
}

int main( int argc, char **argv)
{
	A a1;
	A a2(3);
	A a3 = a2;
	A a4 = (5);
	A *a5 = new A(6);
	changeAge( a5, 22 );
	cout << a1.getAge() << endl;
	cout << a2.getAge() << endl;
	cout << a3.getAge() << endl;
	cout << a5->getAge() << endl;
}


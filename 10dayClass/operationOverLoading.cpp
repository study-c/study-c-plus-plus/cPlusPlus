#include <iostream>

// operation overloading essentially is function

using namespace std;

class A 
{
	public:
		A(){};
		A( int age );
		A operator+( const A &a2 );
		int age;
};

A::A( int age )
{
	this -> age = age;
};

A A::operator+( const A &a2)
{
	A tmp;
	tmp.age = this -> age + a2.age;
	return tmp;
}

int main ( int argc, char ** argv )
{
	A a1(5), a2(6);
	A a3 = a1 + a2;

	cout << a3.age << endl;
	return 0;
}

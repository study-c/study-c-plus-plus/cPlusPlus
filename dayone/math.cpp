#include <iostream>
#include <cmath>
template <typename T>
T twoTimes (T x) {
	return x * 2.0;
}

int main() {
	using namespace std;
	double numOutput;
	cout << "Input number" << endl;
	cin >> numOutput;
	cout << sqrt(numOutput) << endl;	
	cout << twoTimes(numOutput) << endl;
	return 0;
}


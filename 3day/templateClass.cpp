#include <iostream>
#include <string>
#include <cassert>

using namespace std;

template <class T>
T square ( T a ) {
    return a * a;
}

int main (int argc, char * argv[]) {
    int res1 = square(5);
    assert (res1 == 25);
    cout << square(3) << endl;
    cout << square(4.23) << endl;
    int a = 3, b = 5;
    char c ='a', d = 'f';
    return 0;
}
#include <iostream>
#include <string>

using namespace std;

template <typename T>
T square ( T a ) {
    return a * a;
}

template <typename T>
T max_el ( T const& a, T const& b) {
    return a > b ? a : b;
}

int main (int argc, char * argv[]) {
    cout << square(3) << endl;
    cout << square(4.23) << endl;
    int a = 3, b = 5;
    char c ='a', d = 'f';
    cout << max_el(a, b) << endl;
    cout << max_el(c, d) << endl;
    return 0;
}
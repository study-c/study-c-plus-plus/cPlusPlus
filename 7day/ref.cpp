#include <iostream>

using namespace std;

int aa()
{
    int a = 10;
    return a;
}

int& bb ( int &c )
{
    c = 20;
    return c;
}

int& j1()
{
	static int a = 10;
	a ++;
	printf("a:%d \n", a);
	return a;
}

int *j2()
{
	static int a = 10;
	a ++;
	printf("a:%d \n", a);
	return &a;
}


int main( int argc, char **argv)
{
    int c = 3;
    cout << aa() << endl;
    cout << bb( c ) << endl;
    cout << bb(j1()) << endl;
    return 0;
}